// Nomenclatura de variáveis

const fameCategoryList = [
  {
    title: 'User',
    followers: 5
  },
  {
    title: 'Friendly',
    followers: 50,
  },
  {
    title: 'Famous',
    followers: 500,
  },
  {
    title: 'Super Star',
    followers: 1000,
  },
]

export default async function getFameLevelOfGithubUser(req, res) {
  const githubUserName = String(req.query.username)

  if (!githubUserName) {
    return res.status(400).json({
      message: `Please provide an username to search on the github API`
    })
  }

  const githubApiResponse = await fetch(`https://api.github.com/users/${githubUserName}`);

  if (githubApiResponse.status === 404) {
    return res.status(400).json({
      message: `User with username "${github}" not found`
    })
  }

  const userData = await githubApiResponse.json()

  const descendingOrderListByFollowers = fameCategoryList.sort((a, b) => b.followers - a.followers);

  const userFameCategory = descendingOrderListByFollowers.find(category => userData.followers > category.followers)

  const result = {
    github,
    category: userFameCategory.title
  }

  return result
}

getFameLevelOfGithubUser({
  query: {
    username: 'josepholiveira'
  }
}, {})
